package com.example.ahagurulangaugeapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private GridView catView;
    public static List<CategoryModel> catList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        catView=findViewById(R.id.catGridView);
        loadCategories();
        CategoryAdapter categoryAdapter=new CategoryAdapter(catList);
        catView.setAdapter(categoryAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.add_student_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.add_student)
        {
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadCategories() {
        catList.clear();//Here we can use firebase database also in order to fill these rows
        catList.add(new CategoryModel("1","1st Class",4));
        catList.add(new CategoryModel("2","2nd Class",5));
        catList.add(new CategoryModel("3","3rd Class",2));
        catList.add(new CategoryModel("4","4th Class",3));
        catList.add(new CategoryModel("5","5th Class",1));
        catList.add(new CategoryModel("6","6th Class",4));
    }
}