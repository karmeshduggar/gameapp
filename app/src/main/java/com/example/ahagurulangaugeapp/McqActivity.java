package com.example.ahagurulangaugeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class McqActivity extends AppCompatActivity {

    private static final String APIURL ="https://api.thecatapi.com/v1/images/search" ;
    private Button button1,button2,button3,button4,button5,button6;
    private ImageButton left,right;
    private ImageView mcqimageview;
    private LottieAnimationView Animation;
    private static int random=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq);
        //setData();//this method we can use later once we have api link available and replace the link with APIURL

        button1=(Button)findViewById(R.id.option1);
        button2=(Button)findViewById(R.id.option2);
        button3=(Button)findViewById(R.id.option3);
        button4=(Button)findViewById(R.id.option4);
        button5=(Button)findViewById(R.id.option5);
        button6=(Button)findViewById(R.id.option6);
        left=(ImageButton)findViewById(R.id.left);
        right=(ImageButton)findViewById(R.id.right);

       Animation=(LottieAnimationView) findViewById(R.id.animationstar);
        Animation.setVisibility(View.GONE);
        button5.setVisibility(View.GONE);
        button6.setVisibility(View.GONE);
        mcqimageview=(ImageView) findViewById(R.id.img1);

        List<QuestionsModel> list=setDataFromRawJson();

        QuestionsAdpater questionsAdpater=new QuestionsAdpater();
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
      //  buttonClicksHandle();

    }
    private void buttonClicksHandle()
    {
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setBackgroundColor(Color.RED);
                wrongAnimation();  clickableFalse();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button2.setBackgroundColor(Color.RED);
                wrongAnimation();  clickableFalse();
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //there we can add a timer of second and play the animation and move to another question
                rightAnimation();  clickableFalse();
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button4.setBackgroundColor(Color.RED);
                wrongAnimation();  clickableFalse();
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button5.setBackgroundColor(Color.RED);
                wrongAnimation();  clickableFalse();
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button6.setBackgroundColor(Color.RED);
                wrongAnimation();
                clickableFalse();
            }
        });


    }



    private void clickableFalse()
    {
        button1.setClickable(false);
        button2.setClickable(false);
        button3.setClickable(false);
        button4.setClickable(false);
        button5.setClickable(false);
        button6.setClickable(false);

    }
private void wrongAnimation()
{
    Animation.setAnimation(R.raw.wrongans);
    Animation.setVisibility(View.VISIBLE);
}
private void rightAnimation()
{
    Animation.setAnimation(R.raw.staranimation);
    Animation.setVisibility(View.VISIBLE);
}

    private List<QuestionsModel> setDataFromRawJson()
    {
        Resources res=getResources();
        InputStream is=res.openRawResource(R.raw.gameappjson);
        Scanner scanner=new Scanner(is);
        StringBuilder builder=new StringBuilder();
        while(scanner.hasNextLine())
            builder.append(scanner.nextLine());
        List<QuestionsModel> modelList=parseJson(builder.toString());
        return modelList;
    }

    private List<QuestionsModel> parseJson(String s) {
        List<QuestionsModel> questionsModelList = null;
        StringBuilder builder=new StringBuilder();
        try {
            JSONObject root=new JSONObject(s);
            JSONObject testContent=root.getJSONObject("test_content");
            int slidesPerGame=testContent.getInt("slides_per_game");
            JSONArray slides=testContent.getJSONArray("slides");
            for(int i=0;i<slides.length();i++)
            {
                JSONObject firstSlide = slides.getJSONObject(i);
                int slide_id = firstSlide.getInt("slide_id");
                JSONObject slide_game = firstSlide.getJSONObject("slide_game");
                JSONArray card_face = slide_game.getJSONArray("card_face");
                JSONObject imageTextObj = card_face.getJSONObject(0);
                String mcqImageURL = imageTextObj.getString("image");
                Picasso.get().load(mcqImageURL).into(mcqimageview);
                JSONArray options = slide_game.getJSONArray("options");
                setOptions(options);
                QuestionsModel q=new QuestionsModel(mcqImageURL,options.getJSONObject(0),options.getJSONObject(1),options.getJSONObject(2),options.getJSONObject(3),options.getJSONObject(4),)
                questionsModelList.add(q);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }
    private void setOptions(JSONArray jsonArray)
    {
        try {
            JSONObject option1=jsonArray.getJSONObject(0);
            JSONObject option2=jsonArray.getJSONObject(1);
            JSONObject option3=jsonArray.getJSONObject(2);
            JSONObject option4=jsonArray.getJSONObject(3);

            String option1txt=option1.getString("content");//accordingly we can set key of the option and fetch
            String option2txt=option2.getString("content");
            String option3txt=option3.getString("content");
            String option4txt=option4.getString("content");
            button1.setText(option1txt);
            button2.setText(option2txt);
            button3.setText(option3txt);
            button4.setText(option4txt);
            boolean correct=option1.getBoolean("is_correct");
            setListener(correct,button1);
            correct=option2.getBoolean("is_correct");
            setListener(correct,button2);
            correct=option3.getBoolean("is_correct");
            setListener(correct,button3);
            correct=option4.getBoolean("is_correct");
            setListener(correct,button4);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void setListener(boolean correct,Button button)
    {
        if(correct)
            setCorrectListner(button);
        else
            setIncorrectListner(button);

    }

    private void setCorrectListner(Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //there we can add a timer of second and play the animation and move to another question
                rightAnimation();  clickableFalse();
            }
        });


    }
    private void setIncorrectListner(Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setBackgroundColor(Color.RED);
                wrongAnimation();  clickableFalse();
            }
        });
    }


    private void setData()
    {
        RequestQueue queue= Volley.newRequestQueue(this);
        JsonArrayRequest arrayRequest=new JsonArrayRequest(Request.Method.GET, APIURL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("onResponse:",response.toString());
                try {
                    JSONObject jsonObject=response.getJSONObject(0);//first object bundle of the json
                    Log.d("onResponse:",jsonObject.toString());
                    String mcqImageURL=jsonObject.getString("url");//in my api the key is "url" we can set it for other also
                    Picasso.get().load(mcqImageURL).into(mcqimageview);
                    String option1=jsonObject.getString("width");//accordingly we can set key of the option and fetch
                    String option2=jsonObject.getString("height");
                    String option3=jsonObject.getString("id");
                    String option4=jsonObject.getString("width");
                    String option5=jsonObject.getString("width");
                    String option6=jsonObject.getString("width");//if option 6 is not there the view get automatically adjust
                    //later we can add option 4 5 6 according to the needs and change the option acc.

                    button1.setText(option1);
                    button2.setText(option2);
                    button3.setText(option3);
                    button4.setText(option4);
                    Random rand = new Random();
                    int n = rand.nextInt(3);
                    if(n==1)
                        option6="";
                    if(n==2)
                    {
                        option5="";option6="";
                    }
                    if(option5.length()!=0&&option6.length()!=0)//if 6 options are there
                    {
                        button5.setVisibility(View.VISIBLE);
                        button6.setVisibility(View.VISIBLE);
                    }
                    else if(option5.length()!=0)
                    {
                        button5.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(McqActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(arrayRequest);


    }
}