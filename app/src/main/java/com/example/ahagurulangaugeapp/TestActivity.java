package com.example.ahagurulangaugeapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;


public class TestActivity extends AppCompatActivity {

    private RecyclerView testview;
    private Toolbar toolbar;
    private List<TestModel> testList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        testview=(RecyclerView)findViewById(R.id.test_recyclerview);
        toolbar=findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        int cat_index=getIntent().getIntExtra("CAT_INDEX",0);
       getSupportActionBar().setTitle(CategoryActivity.catList.get(cat_index).getName());
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        testview.setLayoutManager(layoutManager);

        loadTestData();
        TestAdapter adapter=new TestAdapter(testList);
        testview.setAdapter(adapter);

    }

    private void loadTestData() {
        testList=new ArrayList<>();
        testList.add(new TestModel("1",50,20));
        testList.add(new TestModel("2",60,10));
        testList.add(new TestModel("3",100,30));
        testList.add(new TestModel("4",40,40));


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            TestActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}