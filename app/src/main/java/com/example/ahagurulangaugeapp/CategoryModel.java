package com.example.ahagurulangaugeapp;

public class CategoryModel {
    private String docID;
    private String name;
    private int numberOfTest;

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfTest() {
        return numberOfTest;
    }

    public void setNumberOfTest(int numberOfTest) {
        this.numberOfTest = numberOfTest;
    }

    public CategoryModel(String docID, String name, int numberOfTest) {
        this.docID = docID;
        this.name = name;
        this.numberOfTest = numberOfTest;
    }
}
