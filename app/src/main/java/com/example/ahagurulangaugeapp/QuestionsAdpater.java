package com.example.ahagurulangaugeapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class QuestionsAdpater extends RecyclerView.Adapter<QuestionsAdpater.ViewHolder> {

    private List<QuestionsModel> questionList;

    public QuestionsAdpater(List<QuestionsModel> questionList) {
        this.questionList = questionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.question_item_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private Button option1,option2,option3,option4,option5,option6;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.quesImg);
            option1=itemView.findViewById(R.id.o1);
            option2=itemView.findViewById(R.id.o2);
            option3=itemView.findViewById(R.id.o3);
            option4=itemView.findViewById(R.id.o4);
            option5=itemView.findViewById(R.id.o5);
            option6=itemView.findViewById(R.id.o6);

        }
        private void setData(final int pos)
        {
            Picasso.get().load(questionList.get(pos).getImageURL()).into(imageView);
            option1.setText(questionList.get(pos).getOption1());
            option2.setText(questionList.get(pos).getOption2());
            option3.setText(questionList.get(pos).getOption3());
            option4.setText(questionList.get(pos).getOption4());
            option5.setText(questionList.get(pos).getOption5());
            option6.setText(questionList.get(pos).getOption6());
        }
    }
}
